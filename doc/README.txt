Versione 1.6.4-0 (21.04.2011) (dd.mm.yyyy)


This dictionary is released under GPL license version 2.0 or above. 
You can find the text of the license on http://www.gnu.org/ (http://www.gnu.org/licenses/gpl.txt)

You can find new Albanian dictionary versions here:
 http://shkenca.org/k6i/

